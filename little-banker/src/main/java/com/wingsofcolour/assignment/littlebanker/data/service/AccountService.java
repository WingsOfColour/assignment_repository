package com.wingsofcolour.assignment.littlebanker.data.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wingsofcolour.assignment.littlebanker.data.dto.AccountDto;
import com.wingsofcolour.assignment.littlebanker.data.entity.Account;
import com.wingsofcolour.assignment.littlebanker.data.service.dao.AccountDao;

@Service
public class AccountService {

	@Autowired
	private AccountDao accountDao;

	@Autowired
	private ModelMapper modelMapper;

	public List<AccountDto> findAll() {
		return accountDao.findAll().stream().map(this::convertToDto).collect(Collectors.toList());
	}

	public AccountDto findById(Long id) {
		return convertToDto(accountDao.findById(id).orElseThrow());
	}

	@Transactional
	public AccountDto save(AccountDto accountDto) {
		return convertToDto(accountDao.save(convertToEntity(accountDto)));
	}

	private AccountDto convertToDto(Account account) {
		return modelMapper.map(account, AccountDto.class);
	}

	private Account convertToEntity(AccountDto accountDto) {
		return modelMapper.map(accountDto, Account.class);
	}
}
