package com.wingsofcolour.assignment.littlebanker.data.service;

import java.sql.Timestamp;
import java.util.Calendar;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wingsofcolour.assignment.littlebanker.data.dto.AccountDto;
import com.wingsofcolour.assignment.littlebanker.data.dto.TransactionDto;
import com.wingsofcolour.assignment.littlebanker.data.entity.Transaction;
import com.wingsofcolour.assignment.littlebanker.data.service.dao.TransactionDao;

@Service
public class TransactionService {

	@Autowired
	private TransactionDao transactionDao;

	@Autowired
	private AccountService accountService;

	@Autowired
	private ModelMapper modelMapper;

	@Transactional
	public TransactionDto updateTransaction(TransactionDto transactionDto) {
		AccountDto accountDtoFrom = accountService.findById(transactionDto.getAccountFromNumber());
		AccountDto accountDtoTo = accountService.findById(transactionDto.getAccountToNumber());

		accountDtoTo.setBalance(accountDtoTo.getBalance().add(transactionDto.getAmount()));
		accountDtoFrom.setBalance(accountDtoFrom.getBalance().subtract(transactionDto.getAmount()));

		accountService.save(accountDtoTo);

		clearRecursionDataIfInvalid(transactionDto);
		transactionDto.setTimestamp(new Timestamp(Calendar.getInstance().getTime().getTime()));
		TransactionDto savedDtoTransaction = convertToDto(transactionDao.save(convertToEntity(transactionDto)));

		accountDtoFrom.getTransactions().add(savedDtoTransaction);

		accountService.save(accountDtoFrom);

		return savedDtoTransaction;
	}

	private TransactionDto clearRecursionDataIfInvalid(TransactionDto transactionDto) {
		if (transactionDto.getRecurring() != null && transactionDto.getRecurring()
				&& transactionDto.getNumberOfDaysTillRecurring() != null) {
			return transactionDto;
		}

		transactionDto.setRecurring(false);
		transactionDto.setNumberOfDaysTillRecurring(null);
		return transactionDto;
	}

	private TransactionDto convertToDto(Transaction transaction) {
		return modelMapper.map(transaction, TransactionDto.class);
	}

	private Transaction convertToEntity(TransactionDto userDto) {
		return modelMapper.map(userDto, Transaction.class);
	}

}
