package com.wingsofcolour.assignment.littlebanker.data.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
public class Transaction {

	@Id
	@GeneratedValue
	private Long transactionId;

	@Nullable
	private String description;

	@NotNull
	@DecimalMin(value = "0.01")
	@Digits(integer = 17, fraction = 2)
	private BigDecimal amount;

	@NotNull
	private Timestamp timestamp;

	@ManyToOne
	@NotNull
	private Account accountFrom;

	@ManyToOne
	@NotNull
	private Account accountTo;

	@Nullable
	private Boolean recurring;

	@Nullable
	private Integer numberOfDaysTillRecurring;

	protected Transaction() {

	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Account getAccountFrom() {
		return accountFrom;
	}

	public void setAccountFrom(Account accountFrom) {
		this.accountFrom = accountFrom;
	}

	public Account getAccountTo() {
		return accountTo;
	}

	public void setAccountTo(Account accountTo) {
		this.accountTo = accountTo;
	}

	public Boolean getRecurring() {
		return recurring;
	}

	public void setRecurring(Boolean recurring) {
		this.recurring = recurring;
	}

	public Integer getNumberOfDaysTillRecurring() {
		return numberOfDaysTillRecurring;
	}

	public void setNumberOfDaysTillRecurring(Integer numberOfDaysTillRecurring) {
		this.numberOfDaysTillRecurring = numberOfDaysTillRecurring;
	}

	@Override
	public String toString() {
		return "Transaction [transactionId=" + transactionId + ", description=" + description + ", amount=" + amount
				+ ", timestamp=" + timestamp + ", accountFrom=" + accountFrom.getAccountNumber() + ", accountTo="
				+ accountTo.getAccountNumber() + ", recurring=" + recurring + ", numberOfDaysTillRecurring="
				+ numberOfDaysTillRecurring + "]";
	}

}
