package com.wingsofcolour.assignment.littlebanker.data.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wingsofcolour.assignment.littlebanker.data.entity.Transaction;

@Repository
public interface TransactionDao extends JpaRepository<Transaction, Long> {

}
