package com.wingsofcolour.assignment.littlebanker.data.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

public class AccountDto {

	private Long accountNumber;

	@NotNull
	@DecimalMin(value = "0.00")
	@Digits(integer = 17, fraction = 2)
	private BigDecimal balance;

	private Timestamp creationTime;

	private List<TransactionDto> transactions;

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public Timestamp getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	public List<TransactionDto> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionDto> transactions) {
		this.transactions = transactions;
	}

	@Override
	public String toString() {
		return "AccountDto [accountNumber=" + accountNumber + ", balance=" + balance + ", creationTime=" + creationTime
				+ ", transactions=" + transactions + "]";
	}

}
