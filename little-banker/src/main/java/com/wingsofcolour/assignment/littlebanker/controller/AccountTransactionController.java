package com.wingsofcolour.assignment.littlebanker.controller;

import java.math.BigDecimal;
import java.net.URI;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.wingsofcolour.assignment.littlebanker.data.dto.AccountDto;
import com.wingsofcolour.assignment.littlebanker.data.dto.TransactionDto;
import com.wingsofcolour.assignment.littlebanker.data.service.AccountService;
import com.wingsofcolour.assignment.littlebanker.data.service.TransactionService;

@RestController
@RequestMapping(value = "/accounts", produces = "application/json")
public class AccountTransactionController {

	@Autowired
	private AccountService accountService;

	@Autowired
	private TransactionService transactionService;

	@GetMapping
	@ResponseBody
	public List<AccountDto> getAllAccounts() {
		return accountService.findAll();
	}
	
	@GetMapping("/{accountNumber}")
	@ResponseBody
	public AccountDto getAccount(@PathVariable Long accountNumber) {
		return accountService.findById(accountNumber);
	}

	@GetMapping("/{accountNumber}/balance")
	@ResponseBody
	public BigDecimal getAccountBalance(@PathVariable Long accountNumber) {
		return accountService.findById(accountNumber).getBalance();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public ResponseEntity<AccountDto> createAccount(@Valid @RequestBody AccountDto accountDto) {
		accountDto.setCreationTime(new Timestamp(Calendar.getInstance().getTime().getTime()));
		AccountDto savedDtoAccount = accountService.save(accountDto);
		URI location = ServletUriComponentsBuilder.fromCurrentRequestUri()
				.path("/{accountNumber}")
				.buildAndExpand(savedDtoAccount.getAccountNumber())
				.toUri();

		return ResponseEntity.created(location).body(savedDtoAccount);
	}

	@PostMapping(path = "/{accountNumber}/transactions")
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public ResponseEntity<TransactionDto> transferFunds(@Valid @RequestBody TransactionDto transactionDto) {
		TransactionDto savedTransaction = transactionService.updateTransaction(transactionDto);

		URI location = ServletUriComponentsBuilder.fromCurrentRequestUri()
				.path("/{transactionId}")
				.buildAndExpand(savedTransaction.getTransactionId())
				.toUri();

		return ResponseEntity.created(location).body(savedTransaction);
	}

	@GetMapping(path = "/{accountNumber}/transactions")
	@ResponseBody
	public List<TransactionDto> getAllAccountTransactions(@PathVariable Long accountNumber,
			@RequestParam(required = false, name = "from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date from,
			@RequestParam(required = false, name = "to") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date to) {
		AccountDto accountDto = accountService.findById(accountNumber);
		return accountDto.getTransactions()
				.stream()
				.filter(t -> filterTransactionByDate(from, to, t))
				.collect(Collectors.toList());
	}

	@GetMapping(path = "/{accountNumber}/transactions/{transactionId}")
	@ResponseBody
	public TransactionDto getAccountTransaction(@PathVariable Long accountNumber, @PathVariable Long transactionId) {
		AccountDto accountDto = accountService.findById(accountNumber);
		return accountDto.getTransactions()
				.stream()
				.filter(t -> t.getTransactionId().equals(transactionId))
				.findFirst()
				.orElseThrow();
	}

	private boolean filterTransactionByDate(Date from, Date to, TransactionDto transactionDto) {
		boolean valid = true;

		if (from != null) {
			valid = transactionDto.getTimestamp().after(from);
		}

		if (to != null && valid) {
			valid = transactionDto.getTimestamp().before(to);
		}

		return valid;
	}

}
