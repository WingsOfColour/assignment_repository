package com.wingsofcolour.assignment.littlebanker.data.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

public class TransactionDto {

	private Long transactionId;

	private String description;

	@NotNull
	@DecimalMin(value = "0.00")
	@Digits(integer = 17, fraction = 2)
	private BigDecimal amount;

	private Timestamp timestamp;

	@NotNull
	private Long accountFromNumber;

	@NotNull
	private Long accountToNumber;

	@Nullable
	private Boolean recurring;

	@Nullable
	private Integer numberOfDaysTillRecurring;

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Long getAccountFromNumber() {
		return accountFromNumber;
	}

	public void setAccountFromNumber(Long accountFromNumber) {
		this.accountFromNumber = accountFromNumber;
	}

	public Long getAccountToNumber() {
		return accountToNumber;
	}

	public void setAccountToNumber(Long accountToNumber) {
		this.accountToNumber = accountToNumber;
	}

	public Boolean getRecurring() {
		return recurring;
	}

	public void setRecurring(Boolean recurring) {
		this.recurring = recurring;
	}

	public Integer getNumberOfDaysTillRecurring() {
		return numberOfDaysTillRecurring;
	}

	public void setNumberOfDaysTillRecurring(Integer numberOfDaysTillRecurring) {
		this.numberOfDaysTillRecurring = numberOfDaysTillRecurring;
	}

	@Override
	public String toString() {
		return "TransactionDto [transactionId=" + transactionId + ", description=" + description + ", amount=" + amount
				+ ", timestamp=" + timestamp + ", accountFromNumber=" + accountFromNumber + ", accountToNumber="
				+ accountToNumber + ", recurring=" + recurring + ", numberOfDaysTillRecurring="
				+ numberOfDaysTillRecurring + "]";
	}

}
