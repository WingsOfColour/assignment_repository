package com.wingsofcolour.assignment.littlebanker.controller;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.wingsofcolour.assignment.littlebanker.data.dto.AccountDto;
import com.wingsofcolour.assignment.littlebanker.data.dto.TransactionDto;
import com.wingsofcolour.assignment.littlebanker.data.service.AccountService;
import com.wingsofcolour.assignment.littlebanker.data.service.TransactionService;
import com.wingsofcolour.assignment.littlebanker.exception.ApplicationResponseEntityExceptionHandler;

@RunWith(MockitoJUnitRunner.class)
public class AccountTransactionControllerTest {

	@Mock
	AccountService accountService;

	@Mock
	TransactionService transactionService;

	@InjectMocks
	AccountTransactionController accountTransactionController;

	private MockMvc mockMvc;

	@Before
	public void configure() {
		mockMvc = MockMvcBuilders.standaloneSetup(accountTransactionController)
				.setControllerAdvice(new ApplicationResponseEntityExceptionHandler())
				.build();
	}

	@Test
	public void whenExistingAccountRetrieved_dataIsCorrectAndStatus200IsReturned() throws Exception {
		AccountDto mockAccount = prepareMockAccountDto(1l, new BigDecimal(4000));

		Mockito.when(accountService.findById(Mockito.anyLong())).thenReturn(mockAccount);
		MockHttpServletResponse response = prepareAndPerformGetRequest("/accounts/1");

		String expected = "{accountNumber: 1, balance: 4000, creationTime: "
				+ mockAccount.getCreationTime().getTime() + ", transactions: []}";
		JSONAssert.assertEquals(expected, response.getContentAsString(), true);
		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}
	
	@Test
	public void whenNonexistentaAccountRetrieved_responseWithErrorWithStatus404Returned() throws Exception {
		Mockito.when(accountService.findById(Mockito.anyLong())).thenThrow(new NoSuchElementException());
		MockHttpServletResponse response = prepareAndPerformGetRequest("/accounts/1");

		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
	}

	@Test
	public void whenAccountInserted_httpStatusCreatedReturnedAndLocationPresent() throws Exception {
		AccountDto mockAccount = prepareMockAccountDto(1l, new BigDecimal(4000));

		Mockito.when(accountService.save(Mockito.any(AccountDto.class))).thenReturn(mockAccount);
		MockHttpServletResponse response = prepareAndPerformPostRequest("/accounts", "{\"balance\": 4000}");

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());
		assertEquals("http://localhost/accounts/1", response.getHeader(HttpHeaders.LOCATION));
	}

	@Test
	public void whenAccountTransactionsWithDateRestrictionRetrieved_transactionSelectedValidAndStatusOkReturned()
			throws Exception {
		TransactionDto earlierTransaction = prepareMockTransactionDto(1l, "description", new BigDecimal(200), Timestamp
				.valueOf("2019-02-10 12:00:00.000"), 1l, 3l);
		TransactionDto laterTransaction = prepareMockTransactionDto(2l, "description", new BigDecimal(400), Timestamp
				.valueOf("2019-02-13 12:00:00.000"), 1l, 2l);
		AccountDto mockAccount = prepareMockAccountDto(1l, new BigDecimal(4000), earlierTransaction, laterTransaction);

		Mockito.when(accountService.findById(Mockito.anyLong())).thenReturn(mockAccount);
		MockHttpServletResponse response = prepareAndPerformGetRequest("/accounts/1/transactions?from=2019-02-11&to=2019-02-14");

		String expected = "[{transactionId: 2, description: description, amount: 400, timestamp:"
				+ laterTransaction.getTimestamp().getTime()
				+ ", accountFromNumber: 1, accountToNumber: 2, recurring: null, numberOfDaysTillRecurring: null}]";
		JSONAssert.assertEquals(expected, response.getContentAsString(), true);
		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}

	@Test
	public void whenNonexistentTransactionRetrieved_responseWithErrorWithStatus404Returned() throws Exception {
		AccountDto mockAccount = prepareMockAccountDto(1l, new BigDecimal(4000));

		Mockito.when(accountService.findById(Mockito.anyLong())).thenReturn(mockAccount);
		MockHttpServletResponse response = prepareAndPerformGetRequest("/accounts/1/transactions/1");

		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
	}

	@Test
	public void whenTransactionCreatedBetweenTwoAccount_responseWithStatusCreatedReturnedAndLocationPresent()
			throws Exception {
		TransactionDto mockedTransaction = prepareMockTransactionDto(1l, "description", new BigDecimal(250), Timestamp
				.valueOf("2019-02-10 12:00:00.000"), 1l, 2l);

		Mockito.when(transactionService.updateTransaction(Mockito.any())).thenReturn(mockedTransaction);
		MockHttpServletResponse response = prepareAndPerformPostRequest("/accounts/1/transactions", "{\"description\": \"description\", \"amount\": 250, \"accountFromNumber\": \"1\", \"accountToNumber\": \"2\"}");

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());
		assertEquals("http://localhost/accounts/1/transactions/1", response.getHeader(HttpHeaders.LOCATION));
	}

	private MockHttpServletResponse prepareAndPerformGetRequest(String url) throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(url).accept(MediaType.APPLICATION_JSON);
		return mockMvc.perform(requestBuilder).andReturn().getResponse();
	}

	private MockHttpServletResponse prepareAndPerformPostRequest(String url, String request) throws Exception {
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post(url)
				.accept(MediaType.APPLICATION_JSON)
				.content(request)
				.contentType(MediaType.APPLICATION_JSON);

		return mockMvc.perform(requestBuilder).andReturn().getResponse();
	}

	private AccountDto prepareMockAccountDto(Long id, BigDecimal balance, TransactionDto... transactions) {
		AccountDto mockAccount = new AccountDto();
		mockAccount.setAccountNumber(id);
		mockAccount.setBalance(balance);
		mockAccount.setCreationTime(new Timestamp(Calendar.getInstance().getTime().getTime()));
		mockAccount.setTransactions(Arrays.asList(transactions));
		return mockAccount;
	}

	private TransactionDto prepareMockTransactionDto(Long id, String description, BigDecimal amount,
			Timestamp timestamp, Long idFrom, Long idTo) {
		TransactionDto transactionDto = new TransactionDto();
		transactionDto.setTransactionId(id);
		transactionDto.setDescription(description);
		transactionDto.setAmount(amount);
		transactionDto.setTimestamp(timestamp);
		transactionDto.setAccountFromNumber(idFrom);
		transactionDto.setAccountToNumber(idTo);
		return transactionDto;
	}

}
